<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset="UTF-8">
	<TITLE>Гарантийный ремонт товаров</TITLE>
	<LINK rel="stylesheet" type="text/css" href="css/style.css">
</HEAD>
<BODY>
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
?>
<HEADER>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><img alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<DIV class="main" style="border: none">
	<TABLE class="menu_employee">
		<CAPTION>Главное меню</CAPTION>
		<TR>
			<TD><A href="/order/add/">Добавить заказ</A></TD>
		</TR>
		<TR>
			<TD><A href="/order/remove/">Удалить заказ</A></TD>
		</TR>
		<TR>
			<TD><A href="/order/edit/">Изменить данные заказа</A></TD>
		<TR>
			<TD><A href="/order/view/">Найти данные о заказе</A></TD>
		</TR>
	</TABLE>
</DIV>
<FOOTER>
	<P>Статус базы данных: <STRONG> <?php checkServer(); ?> </STRONG></P>
</FOOTER>
</BODY>
</HTML>