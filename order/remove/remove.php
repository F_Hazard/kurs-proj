<?php
error_reporting(E_ALL);
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
$get_check = isset($_GET['cid']) && isset($_GET['oid']) && isset($_GET['tid']);
$queries_failed = true;
$CID = $OID = $TID = -1;
$rows_client = [];
$rows_order = [];
if ($get_check) {
	$CID = $_GET['cid'];
	$OID = $_GET['oid'];
	$TID = $_GET['tid'];

	$query_CID = sqlsrv_query($GLOBALS['conn'], d_utf8("SELECT * FROM [Client] WHERE [ID Клиента]=" . $CID));
	$query_OID = sqlsrv_query($GLOBALS['conn'], d_utf8("SELECT * FROM [Order] WHERE [ID Заказа]=" . $OID));
	if ($query_OID && $query_CID) {
		$queries_failed = false;
		$rows_client = sqlsrv_fetch_array($query_CID, SQLSRV_FETCH_NUMERIC);
		$rows_order = sqlsrv_fetch_array($query_OID, SQLSRV_FETCH_NUMERIC);
	}
}
?>
<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset=utf-8>
	<TITLE>Удаление</TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">
	<STYLE>
		table.menu_employee th {
			padding: 15px;
			border: 1px solid black;
		}
	</STYLE>
	<SCRIPT src="/js/base.js"></SCRIPT>
	<SCRIPT src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></SCRIPT>
	<SCRIPT type="text/javascript">
		function checkOrder(startup) {
			let order = document.getElementsByName("client")[0];
			if (order.checked) {
				if (!startup) {
					if (confirm("Будут удалены все данные, связанные с этим клиентом. Продолжить?")) {
						document.getElementsByName("order")[0].checked = true;
						document.getElementsByName("order")[0].disabled = true;
					}
				}
			} else {
				document.getElementsByName("order")[0].disabled = false;
			}
		}
	</SCRIPT>
</HEAD>
<BODY onload="checkOrder(true)">
<HEADER>
	<SECTION class="section messages-section">
		<?php
		console_log(count($rows_client));
		console_log(count($rows_order));
		if (!$get_check)
			echoErr('Ошибка!', 'Не выбран ни один из заказов. Вернитесь на <a href="index.php">страницу выбора заказа</a> и выберите заказ для удаления.');
		else {
			if ($queries_failed)
				echoErr('Ошибка!', 'При получении данных произошла ошибка.');
		}
		?>
	</SECTION>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><IMG alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<?php
if (!$get_check) exit;
?>
<DIV class="main" style="width: 90%">

	<form name="remF" action="remact.php" method="post">
		<TABLE class="menu_employee" style="margin-left: auto; margin-right: auto; border: 2px solid black;">
			<CAPTION>Клиент</CAPTION>
			<TR>
				<TH scope="row">ID Клиента</TH>
				<TH scope="row">ФИО</TH>
				<TH scope="row">Номер телефона</TH>
				<TH scope="row">Удалить?</TH>
			</TR>
			<TR>
				<?php
				foreach ($rows_client as $record)
					echo "<TD>" . e_utf8($record) . "</TD>";

				?>
				<TD><INPUT type="checkbox" name="client" value="<?php echo e_utf8($rows_client[0]); ?>"
						   onchange="checkOrder(false)"></TD>
			</TR>
		</TABLE>
		<TABLE class="menu_employee" style="margin-left: auto; margin-right: auto; border: 2px solid black;">
			<CAPTION>Заказ</CAPTION>
			<TR>
				<TH scope="row">ID Заказа</TH>
				<TH scope="row">ID Техники</TH>
				<TH scope="row">ID Клиента</TH>
				<TH scope="row">Дата заказа</TH>
				<TH scope="row">ID Мастера</TH>
				<TH scope="row">Статус заказа</TH>
				<TH scope="row">Цена</TH>
				<TH scope="row">Удалить?</TH>
			</TR>
			<TR>
				<?php
				foreach ($rows_order as $record) {
					echo "<TD>";
					if(isset($record))
						 echo e_utf8($record);
					else
						echo "-";
					echo "</TD>";

				}
				?>
				<TD><INPUT type="checkbox" name="order" value="<?php echo e_utf8($rows_order[0]); ?>"
						   onchange="checkOrder(false)"></TD>
			</TR>
		</TABLE>
		<INPUT type="submit" class="remove" value="Удалить">
	</FORM>
</DIV>
<FOOTER>
	<P>Статус базы данных: <?php
		checkServer();
		?>
	</P>
</FOOTER>
</BODY>
</HTML>
