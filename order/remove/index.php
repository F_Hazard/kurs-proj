<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset=utf-8>
	<TITLE>Удаление</TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">
	<SCRIPT src="/js/base.js"></SCRIPT>
	<SCRIPT src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></SCRIPT>
</HEAD>
<BODY onload="search()">
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
?>
<HEADER>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><IMG alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<SECTION class="section messages-section">
	<?php
	if (!empty($_POST)) {
		if ($_POST['clientscs'] === 'true' && $_POST['orderscs'] === 'true') {
			echoSuccess("Успешно!", "Данные удалены.", "rscs");
		} else {
			if ($_POST['clientscs'] === 'false') {
				echoErr("Ошибка!", "Не удалось удалить данные клиента.", "rclerr");
			}
			if ($_POST['orderscs'] === 'false') {
				echoErr("Ошибка!", "Не удалось удалить данные заказа.", "roerr");
			}
		}
	}
	?>
</SECTION>
<DIV class="main" style="width: 60%">
	<FORM style="border: none" name="removeForm" onsubmit="return false">
		<label>Введите ФИО:
			<INPUT type="text" name="FIO" oninput="search()">
		</label>
	</FORM>
	<SECTION id="dataArea">
	</SECTION>
</DIV>
<SCRIPT>
	function search() {
		$.ajax({
			url: 'search.php',
			data: {FIO: document.getElementsByName("FIO")[0].value},
			method: "POST",

			success: function (data) {
				document.getElementById("dataArea").innerHTML = data;
			},

			error: function (jqXHR, textStatus) {
				document.getElementById("dataArea").innerHTML =
						"<DIV id='errAJAX' class='result' style='margin-left: auto; margin-right: auto; text-align: center; color: red;'> Ошибка при отправке AJAX-запроса" + textStatus + "</DIV>";
			}
		})
	}

	$(function () {
		$("#Phone").mask("+380 (99) 999-99-99", {});
	});
</SCRIPT>
<FOOTER>
	<P>Статус базы данных: <?php
		checkServer();
		?>
	</P>
</FOOTER>
</BODY>
</HTML>