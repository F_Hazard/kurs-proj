<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
if (!empty($_POST)) {
	$OScs = $CScs = true;
	if (isset($_POST["client"])) {
		$CID = $_POST["client"];
		$TSQL = /** @lang TSQL */
				"DELETE [Client] WHERE [ID Клиента] = " . $CID;
		$query = sqlsrv_query($GLOBALS['conn'], d_utf8($TSQL));
		if (!$query) {
			$CScs = false;
		}
	}
	if (isset($_POST["order"])) {
		$OID = $_POST["order"];
		$TSQL = /** @lang TSQL */
				"DELETE [Order] WHERE [ID Заказа] = " . $OID;
		$query = sqlsrv_query($GLOBALS['conn'], d_utf8($TSQL));
		if (!$query) {
			$OScs = false;
		}
	}
}

?>

<FORM id="remchk" method="post" action="./index.php">
	<INPUT type="hidden" name="clientscs" value="<?php if ($CScs) {
		echo 'true';
	} else {
		echo 'false';
	} ?>">
	<INPUT type="hidden" name="orderscs" value="<?php if ($OScs) {
		echo 'true';
	} else {
		echo 'false';
	} ?>">
</FORM>
<SCRIPT type="text/javascript">
	document.getElementById("remchk").submit();
</SCRIPT>