<?php

include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';

$ID = -1;
if (isset($_GET)) {
	$ID = $_GET['id'];
	$tsql = "SELECT * FROM [Order] INNER JOIN [Client] ON [Order].[ID Клиента] = [Client].[ID Клиента] INNER JOIN [Tech] ON [Order].[ID Техники] = [Tech].[ID Техники] FULL OUTER JOIN Employee ON [Order].[ID Мастера] = Employee.[ID Мастера]  WHERE [ID Заказа] = " . $ID;
	$query = sqlsrv_query($GLOBALS['conn'], d_utf8($tsql));
	$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC, SQLSRV_SCROLL_NEXT);
}
?>
<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset=utf-8>
	<TITLE>Заказ №<?php echo $ID ?></TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">
	<SCRIPT src="/js/base.js"></SCRIPT>
	<SCRIPT src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></SCRIPT>
	<SCRIPT src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></SCRIPT>
	<STYLE>
		th {
			text-align: left;
			padding: 20px 20px 20px 0;
			margin-left: 0;
		}
	</STYLE>
</HEAD>
<BODY>
<?php
console_log("Query worked: " . (($query) ? "Yes" : "No"));
console_log("Query found something: " . ((sqlsrv_has_rows($query)) ? "Yes" : "No"));
console_log("Rows found: " . (sqlsrv_num_rows($query) ? sqlsrv_has_rows($query) : 0));
if (!$query) {
	foreach (sqlsrv_errors() as $error) {
		console_log("Error: " . $error[2]);
	}
}

?>
<HEADER>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><IMG alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<?php
if (!$query) {
	echo "<DIV class=\"alert alert-danger\" id = \"errmsg\">
													<DIV class=\"alert-container\">
														<DIV class=\"alert-icon\">
															<i class=\"fa fa-info-circle\"></i>
														</DIV>
														<button type=\"button\" class=\"close-icon\" onclick=\"document.getElementById('errmsg').remove();\" data-dismiss=\"alert\" aria-label=\"Close\">
															<span>clear</span>
														</button>
														<b class=\"alert-info\">Ошибка:</b> Что-то пошло не так во время выполнения запроса.
													</DIV>
												</DIV></SECTION>";
}
?>
<DIV class="main" style="width: 60%">
	<DIV id="print">
		<TABLE style="width: 100%; margin-right: auto; margin-left: auto;">
			<CAPTION style="font-size: 35px;">Данные заказа №<?php echo $ID ?></CAPTION>
			<tbody>
			<TR>
				<TH scope="col">ФИО Клиента</TH>
				<TD><?php echo e_utf8($data[8]); ?></TD>
			</TR>
			<TR>
				<TH scope="col">Номер телефона клиента</TH>
				<TD><?php echo e_utf8($data[9]); ?></TD>
			</TR>
			<TR>
				<TH scope="col">Наименование техники</TH>
				<TD><?php echo e_utf8($data[11]); ?></TD>
			</TR>
			<TR>
				<TH scope="col">Тип техники</TH>
				<TD><?php echo e_utf8($data[13]); ?></TD>
			</TR>
			<TR>
				<TH scope="col">ФИО Мастера</TH>
				<TD>
					<?php
					if ($data[15]) {
						echo e_utf8($data[15]);
					} else {
						echo "&lt;Нет&gt;";
					}
					?>
				</TD>
				<TH scope="col" style="text-align: center; font-size: 40px; font-weight: bold; padding: 0; margin: 0;">
					Статус заказа
				</TH>
			</TR>
			<TR>
				<TH scope="col">Стоимость ремонта</TH>
				<TD>
					<?php
					if ($data[6]) {
						echo e_utf8($data[6]);
					} else {
						echo "&lt;Не указано&gt;";
					}
					?>
				</TD>
				<TD rowspan="2" style="text-align: center;">
					<?php
					$color = match (e_utf8($data[5])) {
						"Ожидает" => "red",
						"В процессе" => "orange",
						"Готово" => "green",
						default => "black",
					};
					echo "<span style='color: " . $color . "; font-size: 30px; font-weight: bolder;'>" . e_utf8($data[5]) . "</span>";
					?>
				</TD>
			</TR>
			<TR>
				<TH scope="col">Дата поступления заказа</TH>
				<TD><?php echo e_utf8($data[3]) ?></TD>
			</TR>
			</tbody>
		</TABLE>
	</DIV>
	<DIV style="margin-top: 10px; text-align: center">
		<button type="button" onclick="PrintElem('#print');">Распечатать</button>
	</DIV>
	<DIV style="text-align: right"><A href="index.php">Назад</A></DIV>
</DIV>
<FOOTER>
	<P>Статус базы данных: <?php
		checkServer();
		?>
	</P>
</FOOTER>
</BODY>
</HTML>
