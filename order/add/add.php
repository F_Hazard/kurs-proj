<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';

$TID = null;
$CID = 0;
$err_query = '';
$FIO = '';
$Phone = '';
$success = false;
$alreadyExist = false;
if (isset($_POST)) {
	$success = true;
	$TechName = $_POST['TName'];
	$TechType = $_POST['TType'];
	if ($_POST['selNew'] === "new") {
		$FIO = $_POST['FIO'];
		$Phone = $_POST['Phone'];
		$checkQuery = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ d_utf8("SELECT * FROM Client WHERE [ФИО] = '{$FIO}' AND [Номер телефона] = '{$Phone}'"));
		if (!$checkQuery) {
			$err_query = "SELECT * FROM Client WHERE [ФИО] = '{$FIO}' AND [Номер телефона] = '{$Phone}'";
			$success = false;
			goto err;
		}
		elseif (sqlsrv_fetch_array($checkQuery) === null) {
			$query = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ d_utf8("INSERT INTO Client (ФИО, [Номер телефона]) VALUES ('{$FIO}', '{$Phone}')"));
			if (!$query) {
				$err_query = "INSERT INTO Client (ФИО, [Номер телефона]) VALUES ({$FIO}, {$Phone})";
				$success = false;
				goto err;
			}
			$queryID = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ d_utf8("SELECT * FROM Client WHERE ФИО='{$FIO}' AND [Номер телефона]='{$Phone}'"));
			if (!$queryID) {
				$err_query = "SELECT * FROM Client WHERE ФИО='{$FIO}' AND [Номер телефона]='{$Phone}'";
				$success = false;
				goto err;
			}
			$CID = sqlsrv_fetch_array($queryID, SQLSRV_FETCH_NUMERIC)[0];
			$query = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ d_utf8("INSERT INTO Tech ([Наименование техники], Тип, [ID Клиента]) VALUES ('{$TechName}', '{$TechType}', '{$CID}')"));
			if (!$query) {
				$err_query = "INSERT INTO Tech ([Наименование техники], Тип, [ID Клиента]) VALUES ('{$TechName}', '{$TechType}\', '{$CID}')";
				$success = false;
				goto err;
			}
		} else {
			$alreadyExist = true;
			$success = false;
			goto err;
		}
	} else {
		$CID = $_POST['rbtn'];
		$query = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ d_utf8("INSERT INTO Tech ([Наименование техники], [ID Клиента], Тип) VALUES ('{$TechName}', '{$CID}', '{$TechType}')"));
		if (!$query) {
			$err_query = "INSERT INTO Tech ([Наименование техники], [ID Клиента], Тип) VALUES ('{$TechName}', '{$CID}', '{$TechType}')";
			$success = false;
			goto err;
		}
	}
	$query = sqlsrv_query($GLOBALS['conn'], d_utf8(/** @lang TSQL */ "SELECT [ID Техники] FROM Tech WHERE [ID Клиента] = {$CID} AND [Наименование техники] = '{$TechName}'"));
	if (!$query) {
		$err_query = "SELECT [ID Техники] FROM Tech WHERE [ID Клиента] = {$CID} AND [Наименование техники] = '{$TechName}'";
		$success = false;
		goto err;
	}

	$TID = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)[0];
	$query_order = sqlsrv_query($GLOBALS['conn'], d_utf8("INSERT INTO [Order] ([ID Клиента], [ID Техники]) VALUES ({$CID},{$TID})"));
	if (!$query_order) {
		$err_query = "INSERT INTO [Order] ([ID Клиента], [ID Техники]) VALUES ({$CID},{$TID})";
		$success = false;
		goto err;
	}
	if (!$success) {
		err:
		console_log("--------DEBUG INFO--------");
		console_log("-----------TECH-----------");
		console_log("ID: " . $TID);
		console_log("Name: " . $TechName);
		console_log("Type: " . $TechType);
		console_log("Errored query: " . $err_query);
	}
} ?>

<form id="controlForm" method="post" action="./index.php">
	<INPUT type="hidden" name="ctrlCheck" value="<?php if ($success) {
		echo 'true';
	} else {
		echo 'false';
	} ?>">
	<INPUT type="hidden" name="alreadyExist" value="<?php if ($alreadyExist) {
		echo 'true';
	} else {
		echo 'false';
	} ?>">
	<INPUT type="hidden" name="FIO" value="<?php echo $FIO ?>">
	<INPUT type="hidden" name="Phone" value="<?php echo $Phone ?>">
	<INPUT type="hidden" name="ErrQuery" value="<?php echo $err_query; ?>">
</FORM>
<SCRIPT type="text/javascript">
	document.getElementById("controlForm").submit();
</SCRIPT>