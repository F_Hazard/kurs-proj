<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD lang="ru">
	<META charset=utf-8>
	<TITLE>Добавление</TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">

	<SCRIPT src="/js/base.js"></SCRIPT>
	<SCRIPT src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></SCRIPT>
	<SCRIPT src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></SCRIPT>
	<STYLE>
		table {
			display: inline-block;
			vertical-align: top;
		}
	</STYLE>
	<SCRIPT>
		$(function () {
			$("#Phone").mask("+380 (99) 999-99-99", {});
		});
	</SCRIPT>
</HEAD>
<BODY onload="selNewFnc()">
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
error_reporting(E_ALL);
?>
<HEADER>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><IMG alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<SECTION class="section messages-section">
	<?php

	echoSuccess('Успешно!', 'Заказ добавлен.', 'addScs');
	if (isset($_POST['ctrlCheck'])) {
		if ($_POST['ctrlCheck'] === 'true')
			echoSuccess('Успешно!', 'Заказ добавлен.', 'addScs');
		else {
			if ($_POST['alreadyExist'] === 'true')
				echoErr('Ошибка! ', "Клиент {$_POST['FIO']} с номером телефона {$_POST['Phone']} уже существует.", 'adAEErr');
			elseif (sqlsrv_errors() != null)
				echoErr('Ошибка! ', "Ошибка в запросе к базе данных.", 'addQErr');
			else
				echoErr('Ошибка! ', "Неизвестная ошибка.", 'addUErr');
			console_log($_POST['ErrQuery']);
		}
	}
	?>
</SECTION>
<DIV class="main" style="margin-left: auto; margin-right: auto; align-self: center; width: 800px">
	<FORM method="POST" action="add.php">

		<TABLE style="display: inline-block; height: 100%; margin: 0; padding: 0; text-align: left">
			<CAPTION>Клиент</CAPTION>
			<TR>
				<TD colspan="2">
					<SELECT style="width: 100%" id="selNew" name="selNew" onchange="selNewFnc()">
						<OPTION value="new">Внести данные клиента в базу данных</OPTION>
						<OPTION value="exs">Клиент уже есть в базе данных</OPTION>
					</SELECT>
				</TD>
			</TR>
			<TR class="ClientNew">
				<TD>ФИО</TD>
				<TD><INPUT id="FIO" name="FIO" type="text"></INPUT></TD>
			</TR>
			<TR class="ClientNew">
				<TD>Номер телефона</TD>
				<TD><INPUT id="Phone" name="Phone" type="text"></INPUT></TD>
			</TR>
			<TR class="ClientExs">
				<TD> <?php
					getExsClients() ?> </TD>
			</TR>
		</TABLE>

		<TABLE style="display: inline-block; height: 100%; margin: 0; padding: 0;">
			<CAPTION>Техника</CAPTION>
			<TR>
				<TD>Наименование</TD>
				<TD><INPUT id="TName" name="TName" type="text"/></TD>
			</TR>
			<TR>
				<TD>Тип</TD>
				<TD><INPUT id="TType" name="TType" type="text"/></TD>
			</TR>
		</TABLE>
		<DIV style="text-align: center"><INPUT type="submit" value="Внести изменения"/></DIV>
	</FORM>
</DIV>
<FOOTER>
	<P>Статус базы данных: <?php
		checkServer();
		?>
	</P>
</FOOTER>
</BODY>
</HTML>