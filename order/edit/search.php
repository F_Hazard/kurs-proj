<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';

function searchClients($ClientFIO)
{
    $html = '';
    $tsql_clients = /** @lang TSQL */
        "SELECT [Order].[ID Заказа], Client.ФИО, Client.[Номер телефона], Tech.[Наименование техники], [Order].[Дата заказа], [Order].[Статус заказа] FROM [Order] INNER JOIN Client ON [Order].[ID Клиента] = [Client].[ID Клиента] INNER JOIN Tech ON [Order].[ID Техники] = [Tech].[ID Техники] WHERE ФИО LIKE '{$ClientFIO}%'";
    if ($ClientFIO === '') {
        $tsql_clients = /** @lang TSQL */
            "SELECT [Order].[ID Заказа], Client.ФИО, Client.[Номер телефона], Tech.[Наименование техники], [Order].[Дата заказа], [Order].[Статус заказа] FROM [Order] INNER JOIN Client ON [Order].[ID Клиента] = [Client].[ID Клиента] INNER JOIN Tech ON [Order].[ID Техники] = [Tech].[ID Техники]";
    }

    $control = sqlsrv_query($GLOBALS['conn'], d_utf8($tsql_clients));
    if ($control === false) {
        return;
    }
    if (sqlsrv_has_rows($control)) {
        echo /** @lang HTML */ "<TABLE id='table_edt' class='sort' style = \"margin-left: auto; margin-right: auto; border: 2px solid black;\"><TR>" .
            "<TH style='border: black solid 1px; text-align: center'>ID Заказа</TH>" .
            "<TH style='border: black solid 1px; text-align: center'>ФИО Клиента</TH>" .
            "<TH style='border: black solid 1px; text-align: center'>Номер телефона</TH>" .
            "<TH style='border: black solid 1px; text-align: center'>Наименование техники</TH>" .
            "<TH style='border: black solid 1px; text-align: center'>Дата поступления заказа</TH>" .
            "<TH style='border: black solid 1px; text-align: center'>Статус заказа</TH></TR>";

        while ($row = sqlsrv_fetch_array($control, SQLSRV_FETCH_NUMERIC)) {
            $color = match (e_utf8($row[5])) {
                "Ожидает" => "red",
                "В процессе" => "orange",
                "Готово" => "green",
                default => "black",
            };
            $style = "border: black solid 1px;";
            echo /** @lang HTML */ "<TR title='Нажмите на строку чтобы изменить данные' onclick='window.location.href=\"edit.php?id={$row[0]}\";return false;' style='text-align: center'>\n" .
                "<TD style='border: black solid 1px'>" . e_utf8($row[0]) . "</TD>\n" .
                "<TD style='{$style}'>" . e_utf8($row[1]) . "</TD>\n" .
                "<TD style='{$style}'>" . e_utf8($row[2]) . "</TD>\n" .
                "<TD style='{$style}'>" . e_utf8($row[3]) . "</TD>\n" .
                "<TD style='{$style}'>" . e_utf8($row[4]) . "</TD>\n" .
                "<TD style='color: {$color}; {$style}'>" . e_utf8($row[5]) . "</TD>\n" .
                "</TR>";
        }
        echo "</TABLE>";
    } else {
        echo "<DIV id='notFound' style='margin-left: auto; margin-right: auto; text-align: center; color: red;'> Не найдено ни одного килента с таким ФИО </DIV>";
    }
}

searchClients($_POST['FIO']);
?>
