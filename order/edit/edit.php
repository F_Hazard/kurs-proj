<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
$ID = -1;
$data = [];
if (isset($_GET['id'])) {
	$ID = $_GET['id'];
	$query = sqlsrv_query($GLOBALS['conn'], d_utf8("SELECT * FROM [Order] INNER JOIN [Client] ON [Order].[ID Клиента] = [Client].[ID Клиента] INNER JOIN [Tech] ON [Order].[ID Техники] = [Tech].[ID Техники] FULL OUTER JOIN Employee ON [Order].[ID Мастера] = Employee.[ID Мастера]  WHERE [ID Заказа] = " . $ID));
	if ($query)
		$data = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC, SQLSRV_SCROLL_NEXT);
}
?>
<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset=utf-8>
	<TITLE>Изменение данных заказа №<?php echo $ID ?></TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">
	<SCRIPT src="/js/base.js"></SCRIPT>
	<SCRIPT src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></SCRIPT>
	<SCRIPT src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></SCRIPT>
	<STYLE>
		form#editForm {
			border: none;
		}
		th {
			text-align: left;
			padding: 20px 20px 20px 0;
			margin-left: 0;
		}
		input[type=text], input[type=number] {
			width: 300px;
		}
	</STYLE>
	<SCRIPT type="text/javascript">
		$(function () {
			$("#Phone").mask("+380 (99) 999-99-99", {});
		});

		function startup() {
			const StatusID =
					<?php
					$StatusID = match(e_utf8($data[5])) {
						"Ожидает" => 0,
						"В процессе" => 1,
						"Готово" => 2,
						default => -1,
					};
					echo $StatusID;
					?>;
			$(`#SelStatus`).val(StatusID);
			$("#Master").val(<?php if ($data[14]) {
				echo $data[14];
			} else {
				echo "-1";
			}?>);
		}
	</SCRIPT>
</HEAD>
<BODY onload="startup()">
<SECTION class="section messages-section">
	<?php
	if (empty($data)) {
		echoErr('Ошибка!', 'При получении данных произошла ошибка.');
		foreach (sqlsrv_errors() as $error) {
			console_log("Error: " . $error[2]);
		}
	}
	?>
</SECTION>
<HEADER>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><IMG alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<SECTION class="section messages-section">
	<?php
	if (!$query || (!empty($_POST) && $_POST['success'] === "false")) {
		if (!$query)
			echoErr("Ошибка!", "Не удалось получить данные.", "edterrg");

		if (!empty($_POST) && $_POST['success'] === "false")
			echoErr("Ошибка!", "Не удалось отправить данные.", "edterrs");
	} elseif (!empty($_POST) && $_POST['success'] === "true") {
		echoErr("Успешно!", "Данные изменены.", "edtscs");
	}
	?>
</SECTION>
<DIV class="main" style="width: 60%">
	<DIV id="print">
		<FORM id="editForm" action="editaction.php" method="post">
			<TABLE style="width: 100%; margin-right: auto; margin-left: auto;">
				<CAPTION style="font-size: 35px;">Данные заказа №<?php echo $ID ?></CAPTION>
				<TR>
					<TH scope="col"><LABEL for="FIO">ФИО Клиента</LABEL></TH>
					<TD><INPUT name="FIO" id="FIO" type="text" value="<?php echo e_utf8($data[8]); ?>"/></TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="Phone">Номер телефона клиента</LABEL></TH>
					<TD><INPUT name="Phone" id="Phone" type="text" value="<?php echo e_utf8($data[9]); ?>"/></TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="TName">Наименование техники</LABEL></TH>
					<TD><INPUT name="TName" id="TName" type="text" value="<?php echo e_utf8($data[11]); ?>"/></TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="TType">Тип техники</LABEL></TH>
					<TD><INPUT name="TType" id="TType" type="text" value="<?php echo e_utf8($data[13]); ?>"/></TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="Master">ФИО Мастера</LABEL></TH>
					<TD>
						<SELECT id="Master" name="Master">
							<OPTION value="-1">&lt;Нет&gt;</OPTION>
							<?php
							$tsqlMas = /** @lang TSQL */
									"SELECT * FROM Employee";
							$queryMas = sqlsrv_query($GLOBALS['conn'], d_utf8($tsqlMas));
							while ($row = sqlsrv_fetch_array($queryMas, SQLSRV_FETCH_NUMERIC))
								echo "<OPTION value='.$row[0].'>". e_utf8($row[1])."</OPTION>";
							?>
					</TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="Cost">Стоимость ремонта</LABEL></TH>
					<TD><INPUT name="Cost" id="Cost" type="number" min="0" value=
								<?php
								if ($data[6]) {
									echo e_utf8($data[6]);
								} else {
									echo "0";
								}
								?>>
					</TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="OrderDate">Дата поступления заказа</LABEL></TH>
					<TD><INPUT id="OrderDate" name="OrderDate" type="date" value="<?php echo e_utf8($data[3]) ?>"/></TD>
				</TR>
				<TR>
					<TH scope="col"><LABEL for="SelStatus">Статус заказа</LABEL></TH>
					<TD>
						<SELECT style="padding: 5px" id="SelStatus" name="SelStatus">
							<option value="0">Ожидает</option>
							<option value="1">В процессе</option>
							<option value="2">Готово</option>
						</SELECT>
					</TD>
				</TR>
			</TABLE>
			<INPUT type="hidden" name="orderid" value="<?php echo $ID ?>"/>
			<INPUT type="hidden" name="techid" value="<?php echo $data[1] ?>"/>
			<INPUT type="hidden" name="clientid" value="<?php echo $data[2] ?>"/>
			<DIV style="text-align: center"><INPUT type="submit" value="Внести изменения"/></DIV>
		</FORM>
	</DIV>
	<DIV style="text-align: right"><A href="index.php">Назад</A></DIV>
</DIV>
<FOOTER>
	<P>Статус базы данных: <?php
		checkServer();
		?>
	</P>
</FOOTER>
</BODY>
</HTML>
