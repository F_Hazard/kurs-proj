<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';

$success = true;
if (isset($_POST)) {
	$FIO = $_POST['FIO'];
	$Phone = $_POST['Phone'];
	$TechName = $_POST['TName'];
	$TechType = $_POST['TType'];
	$Cost = $_POST['Cost'];
	$Date = $_POST['OrderDate'];
	$Status = match ($_POST['SelStatus']) {
		1 => "В процессе",
		2 => "Готово",
		default => "Ожидает",
	};
	$OID = $_POST['orderid'];
	$CID = $_POST['clientid'];
	$TID = $_POST['techid'];
	if ($_POST['Master'] === '-1') {
		$MID = "NULL";
	} else {
		$MID = $_POST['Master'];
	}

	$TsqlClient = /** @lang TSQL */
			"UPDATE [Client] SET [ФИО]='{$FIO}', [Номер телефона]='{$Phone}' WHERE [ID Клиента]={$CID}";
	$TsqlTech = /** @lang TSQL */
			"UPDATE [Tech] SET [Наименование техники]='{$TechName}', [Тип]='{$TechType}' WHERE [ID Техники]={$TID}";
	$TsqlOrder = /** @lang TSQL */
			"UPDATE [Order] SET [Дата заказа]='{$Date}', [Цена]='{$Cost}', [Статус заказа]='{$Status}', [ID Мастера]={$MID} WHERE [ID Заказа]={$OID}";
	$query = sqlsrv_query($GLOBALS['conn'], d_utf8($TsqlClient));
	if (!$query) {
		$success = false;
		echo "Client Failed";
		console_log($TsqlClient);
	}
	if ($success) {
		$query = sqlsrv_query($GLOBALS['conn'], d_utf8($TsqlTech));
	}
	if (!$query) {
		$success = false;
		echo "Tech Failed";
		console_log($TsqlTech);
	}
	if ($success) {
		$query = sqlsrv_query($GLOBALS['conn'], d_utf8($TsqlOrder));
	}
	if (!$query) {
		$success = false;
		echo "Order Failed";
		console_log($TsqlOrder);
	}
	if (!$success) {
		foreach (sqlsrv_errors() as $sqlsrv_error) {
			console_log(e_utf8($sqlsrv_error[2]));
		}
	}
}
?>
<form id="return" action="./edit.php?id=<?php echo $OID ?>" method="post">
	<INPUT type="hidden" name="success" value="<?php if ($success) {
		echo 'true';
	} else {
		echo 'false';
	} ?>"/>
</FORM>
<SCRIPT type="text/javascript">
	document.getElementById("return").submit();
</SCRIPT>