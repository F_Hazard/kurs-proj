let header = document.createElement("tr");
let table, caller;

function PrintElem(elem) {
    Popup($(elem).html());
}

function Popup(data) {
    var mywindow = window.open('', 'print doc', 'height=400,width=600');
    mywindow.document.write('<HTML lang="ru"><HEAD><TITLE>Печать заказа</TITLE>');
    mywindow.document.write(
        '\t\t<STYLE >\n' +
        '\t\tbody' +
        '    {' +
        '       zoom: 154%' +
        '    }' +
        '\t\t\tth{\n' +
        '\t\t\t\ttext-align: left;\n' +
        '\t\t\t\tpadding:    20px 20px 20px 0;\n' +
        '\t\t\t\tmargin-left: 0;\n' +
        '\t\t\t}\n' +
        '\t\t\ttable caption\n' +
        '\t\t\t{\n' +
        '\t\t\t\tmargin-top: 10px;\n' +
        '\t\t\t }\n' +
        'body\n' +
        '{\n' +
        '    background-color: white;\n' +
        '}\n' +
        '\t\t</STYLE>\n'
    )
    mywindow.document.write('</HEAD><BODY>');
    mywindow.document.write(data);
    mywindow.document.write('</BODY></HTML>');
    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10
    mywindow.print();
    mywindow.close();
    return true;
}

function loadTableHeaders(callerid, tableid) {
    caller = document.getElementById(callerid);
    table = document.getElementById(tableid);
    table.setAttribute("cellpadding", "4px");
    header.remove();
    let selected = caller.selectedIndex;
    switch (selected) {
        case 0:
            header.innerHTML = "<TH>ID</TH>" +
                "<TH>ФИО</TH>" +
                "<TH>Номер телефона</TH>" +
                "<TH>ID товаров</TH>";
            break;
        case 1:
            header.innerHTML = "<TH>ID</TH>" +
                "<TH>Наименование</TH>" +
                "<TH>ID клиента</TH>";
            break;
    }
    table.append(header);
}

function selNewFnc() {
    let sel = document.getElementById("selNew");
    let clientExs = document.getElementsByClassName("ClientExs");
    let clientNew = document.getElementsByClassName("ClientNew");
    if (sel.value === "exs") {
        for (let clientNewKey in clientNew) clientNew[clientNewKey].hidden = true;
        for (let clientExsKey in clientExs) clientExs[clientExsKey].hidden = false;
    } else {
        for (let clientNewKey in clientNew) clientNew[clientNewKey].hidden = false;
        for (let clientExsKey in clientExs) clientExs[clientExsKey].hidden = true;
    }
}