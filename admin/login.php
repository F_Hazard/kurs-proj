<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';

$login = $_POST['login'];
$password = md5($_POST['password']);
$ql = sqlsrv_fetch_array(sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ "SELECT * FROM Admin_Settings WHERE Setting_name = 'admin_uname'"), SQLSRV_FETCH_ASSOC)['Setting_value'];
$qpwd = sqlsrv_fetch_array(sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ "SELECT * FROM Admin_Settings WHERE Setting_name = 'admin_pwd'"), SQLSRV_FETCH_ASSOC)['Setting_value'];

if ($qpwd === $password && $ql === $login) {
    $open_session_query = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ "INSERT INTO Sessions(user_ip, user_name) VALUES ({$_SERVER['REMOTE_ADDR']}, {$login})");
    echo "  <form id=\"redir\" method='post' action=\"./dashboard/index.php\">
                    <INPUT type='hidden' name='loginFailed' value='false'/>
                    <INPUT type='hidden' name='login' value='" . $login . "'/>
                </FORM>
                <SCRIPT type=\"text/javascript\">
                    document.getElementById(\"redir\").submit();
                </SCRIPT>";
} else {
    echo "  <form id=\"redir\" method='post' action=\"./index.php\">
            <INPUT type='hidden' name='loginFailed' value='true'/>
          </FORM>
          <SCRIPT type=\"text/javascript\">
            document.getElementById(\"redir\").submit();
          </SCRIPT>";
}