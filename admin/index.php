<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset="UTF-8">
	<TITLE>Гарантийный ремонт товаров</TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">
</HEAD>
<BODY>
<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';
if ($_POST['loginFailed'] === 'true') {
	$_POST['loginFailed'] = 'false';
	echoErr("Ошибка!", "Неверный логин или пароль.", "incorrectLogin");
	echo "</SECTION>";
}
?>
<HEADER>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><img alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<DIV class="main" style="border: none">
	<form id="admin_login" method="post" action="login.php">
		<TABLE style="display: inline-block; height: 100%; margin: 0; padding: 0;">
			<CAPTION style="font-size: 19pt">Панель администратора</CAPTION>
			<TR>
				<TD><label for="login">Логин</label></TD>
				<TD><INPUT id="login" name="login" type="text"></INPUT></TD>
			</TR>
			<TR>
				<TD><label for="password">Пароль</label></TD>
				<TD><INPUT id="password" name="password" type="password"></INPUT></TD>
			</TR>
		</TABLE>
		<DIV style="text-align: center"><INPUT type="submit" value="Вход"/></DIV>
	</FORM>
</DIV>
<FOOTER>
	<P>Статус базы данных: <STRONG> <?php checkServer(); ?> </STRONG></P>
</FOOTER>
</BODY>
</HTML>