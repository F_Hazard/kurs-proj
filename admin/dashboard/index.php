<?php
include $_SERVER['DOCUMENT_ROOT'] . '/php/funcs.inc';

error_reporting(E_ALL);
$loggedIn = false;
$sessionOpen = false;
if ($_POST['loginFailed'] == 'false')
	$loggedIn = true;
if ($loggedIn) {
	$sessionOpen = session_start([
			'cookie_lifetime' => 180,
			'name' => $_POST['login'],
	]);
	if ($sessionOpen) {
		$login = $_POST['login'];
		$_SESSION['username'] = $login;
	}
}
?>

<!DOCTYPE HTML>
<HTML lang="ru">
<HEAD>
	<META charset="UTF-8">
	<TITLE>Гарантийный ремонт товаров</TITLE>
	<LINK rel="stylesheet" type="text/css" href="/css/style.css">
	<LINK rel="stylesheet" type="text/css" href="/css/warnings.css">
	<SCRIPT src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></SCRIPT>

</HEAD>
<BODY>
<HEADER>
	<SECTION class="section messages-section">
	</SECTION>
	<DIV id="header">
		<H1>Гарантийный ремонт товаров</H1>
		<H2>Панель управления</H2>
		<A href="/" id="logo"><img alt="Logo" width="150px" src="/img/logo.png"></A>
	</DIV>
</HEADER>
<DIV class="main" style="border: none">
	<?php
	if ($sessionOpen)
		echoSuccess("Успешно!", "Сессия запущена.", "sessmsg");
	else
		echoErr("Ошибка!", "Не удалось запустить сессию.", "sessmsg");

	?>
</DIV>
<FOOTER>
	<P>Статус базы данных: <STRONG> <?php checkServer(); ?> </STRONG></P>
</FOOTER>
</BODY>
</HTML>
