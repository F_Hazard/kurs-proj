<?php
include $_SERVER['DOCUMENT_ROOT'] . "/php/config.inc";


if (!function_exists('e_utf8')) {
    /**
     * Converts ASCII string to UTF-8 string
     * @param string $string <P>
     * The string to be converted
     * </P>
     * @return string|false Converted string or false on failure
     */
    function e_utf8(string $string): bool|string
    {
        return iconv('windows-1251', 'utf-8', $string);
    }
}
if (!function_exists('d_utf8')) {
    /**
     * Converts UTF-8 string to ASCII string
     * @param string $string <P>
     * The string to be converted
     * </P>
     * @return string|false Converted string or false on failure
     */
    function d_utf8(string $string): bool|string
    {
        return iconv('utf-8', 'windows-1251', $string);
    }
}
if (!function_exists('console_log')) {
    /**
     * Creates JS' console.log with string
     * @param string $data <P>
     * The string to put into console.log
     * </P>
     * @return void
     */
    function console_log(string $data)
    {
        $data = str_replace(array('"', '\''), array('\\"', '\\\''), $data);
        echo /** @lang HTML */ "<SCRIPT> console.log('" . $data . "'); </SCRIPT>\n";
    }
}

if (!function_exists('checkServer')) {
    /**
     * <P> Check and echo whether the server is online </P>
     */
    function checkServer()
    {
        if ($GLOBALS['conn']) echo '<span style="color: green">Доступна</span>';
        else echo '<span style = "color: red">Недоступна</span>';
    }
}

if (!function_exists('processViewReserve')) {
    /**
     * @deprecated
     * Unused for now.
     */
    function processViewReserve()
    {
        $table = $_POST["tableselect"];
        $mainquery = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ "SELECT * FROM $table");
        if (!$mainquery) {
            echo /** @lang HTML */ "<BR><span style = \"color: red\"><b>Ошибка запроса. Проверьте состояние базы данных и попробуйте позже.</b></span>";
        } else {
            echo /** @lang HTML */ "<TABLE id = \"resultingtbl\" align = center border = 1px cellspacing = 0 style=\"min-width: 400px; margin-bottom: 30px;\">";
            echo "<TR>";
            $columns = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '$table'");
            while ($row = sqlsrv_fetch_array($columns, SQLSRV_FETCH_NUMERIC)) {
                $row[0] = e_utf8($row[0]);
                echo /** @lang HTML */ "<TH>$row[0]</TH>";
            }
            echo "	</TR>";
            while ($row = sqlsrv_fetch_array($mainquery, SQLSRV_FETCH_NUMERIC)) {
                {
                    echo /** @lang HTML */ "<TR>";
                    for ($i = 0; isset($row[$i]); $i++) {
                        $row[$i] = e_utf8($row[$i]);
                        echo /** @lang HTML */ "<TD>$row[$i]</TD>";
                    }
                    echo /** @lang HTML */ "</TR>";
                }
            }
            echo /** @lang HTML */ "</TABLE>";
        }
    }
}

if (!function_exists("getExsClients")) {
    function getExsClients()
    {
        $query = sqlsrv_query($GLOBALS['conn'], /** @lang TSQL */ "SELECT * FROM Client");
        if (!$query) {
            echo /** @lang HTML */ "<BR><span style = \"color: red\"><b>Ошибка запроса. Проверьте состояние базы данных и попробуйте позже.</b></span>";
        } else {
            echo /** @lang HTML */ "<TABLE style='border: black thick;'>";
            echo /** @lang HTML */ "<TR><TH>ФИО</TH><TH>Номер телефона</TH><TH>Выбор</TH></TR>";
            while ($row = sqlsrv_fetch_array($query, SQLSRV_FETCH_NUMERIC)) {
                $row[0] = e_utf8($row[0]);
                $row[1] = e_utf8($row[1]);
                $row[2] = e_utf8($row[2]);
                echo /** @lang HTML */ "<TR><TD>" . $row[1] . "</TD><TD>" . $row[2] . "</TD><TD><INPUT type='radio' name='rbtn' id='rbtn' value = " . $row[0] . " checked></TD></TR>";
            }
            echo /** @lang HTML */ "</TABLE>";
        }
    }
}

if (!function_exists("echoSuccess")) {
    function echoSuccess($header, $message, $id = null)
    {
        if ($id)
            echo "<DIV class=\"alert alert-success scsmsg\" id = \"" . $id . "\">
                                        <DIV class=\"alert-container\">
                                            <DIV class=\"alert-icon\">
                                                <i class=\"fa fa-check\"></i>
                                            </DIV>
                                            <button type=\"button\" class=\"close-icon\" data-dismiss=\"alert\" aria-label=\"Close\" onclick='document.getElementById(\"" . $id . "\").remove();'>
                                                <span>clear</span>
                                            </button>
                                            <b class=\"alert-info\">" . $header . "</b>" . $message .
                                        "</DIV>
                                    </DIV>";
        else
            echo "<DIV class=\"alert alert-success scsmsg\">
                                        <DIV class=\"alert-container\">
                                            <DIV class=\"alert-icon\">
                                                <i class=\"fa fa-check\"></i>
                                            </DIV>
                                            <b class=\"alert-info\">" . $header . "</b>" . $message .
                "</DIV>
                                    </DIV>";
    }
}
if (!function_exists("echoErr")) {
    function echoErr($header, $message, $id = null)
    {
        if ($id)
            echo "<DIV class=\"alert alert-danger errmsg\" id=\"" . $id . "\">
										<DIV class=\"alert-container\">
											<DIV class=\"alert-icon\">
												<i class=\"fa fa-info-circle\"></i>
											</DIV>
											<button type=\"button\" class=\"close-icon\" onclick='document.getElementById(\"" . $id . "\").remove();'>
												<span>clear</span>
											</button>
											<b class=\"alert-info\">" . $header . "</b>" . $message .
                                        "</DIV>
									</DIV>";
        else
            echo "<DIV class=\"alert alert-danger errmsg\" >
										<DIV class=\"alert-container\">
											<DIV class=\"alert-icon\">
												<i class=\"fa fa-info-circle\"></i>
											</DIV>
											<b class=\"alert-info\">" . $header . "</b>" . $message .
                                        "</DIV>
									</DIV>";
    }
}
if (!function_exists("echoWarning")) {
    function echoWarning($header, $message, $id = null)
    {
        if ($id)
            echo "<DIV class=\"alert alert-warning wmsg\" id=\"" . $id . "\">
										<DIV class=\"alert-container\">
											<DIV class=\"alert-icon\">
												<i class=\"fa fa-info-circle\"></i>
											</DIV>
											<button type=\"button\" class=\"close-icon\" onclick='document.getElementById(\"" . $id . "\").remove();'>
												<span>clear</span>
											</button>
											<b class=\"alert-info\">" . $header . "</b>" . $message .
                "</DIV>
									</DIV>";
        else
            echo "<DIV class=\"alert alert-warning wmsg\" >
										<DIV class=\"alert-container\">
											<DIV class=\"alert-icon\">
												<i class=\"fa fa-info-circle\"></i>
											</DIV>
											<b class=\"alert-info\">" . $header . "</b>" . $message .
                "</DIV>
									</DIV>";
    }
}
